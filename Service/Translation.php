<?php
/*
 * This file is part of the CwdTranslationAdminBundle.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\TranslationAdminBundle\Service;

use Asm\TranslationLoaderBundle\Doctrine\TranslationManager;
use Cwd\GenericBundle\BaseService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Asm\TranslationLoaderBundle\Entity\Translation as Entity;
use JMS\DiExtraBundle\Annotation as DI;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\Session;
use Cwd\TranslationAdminBundle\Service\Exception\TranslationNotFoundException as NotFoundException;



/**
 * Instance Service Class
 *
 * @author Ludwig Ruderstaller <lr@cwd.at>
 * @DI\Service("service_translation")
 * @SuppressWarnings("ShortVariable")
 */
class Translation extends \ITAsset\Service\BaseService
{
    /**
     * @param EntityManager      $entityManager
     * @param Logger             $logger
     *
     * @DI\InjectParams({
     * });
     */
    public function __construct(EntityManager $entityManager, Logger $logger)
    {
        $this->entityManager     = $entityManager;
        $this->logger = $logger;
    }

    /**
     * Find Object by ID
     *
     * @param int $id
     *
     * @return \ITAsset\Model\Entity\Instance
     * @throws NotFoundException
     */
    public function find($id)
    {
        $obj = $this->translationManager->findTranslationBy(array(
            'transKey' => $id,
            'transLocale' => 'de',
            'messageDomain' => 'messages'
        ));

        if ($obj === null) {
            $this->getLogger()->info('Row with ID {id} not found', array('id' => $id));
            throw new NotFoundException('Row with ID '.$id.' not found');
        }

        return $obj;
    }

    /**
     * Find rows by ID
     * @param array $ids
     *
     * @return ArrayCollection
     *
     * @throws NotFoundException
     */
    public function findByArray($ids)
    {
        try {
            $carts = $this->getEm()->getRepository('Asm\TranslationLoaderBundle\Entity\Translation')->findByArray($ids);
        } catch (\Exception $e) {
            $s = implode(', ', $ids);
            $this->getLogger()->error('Rows with IDs {id} not found', array('id' => $s));
            throw new NotFoundException('Rows with IDs '.$s.' not found');
        }

        return $carts;
    }

    /**
     * @return Entity
     */
    public function getNew()
    {
        return new Entity();
    }
}