<?php
/*
 * This file is part of the CwdTranslationAdminBundle
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\TranslationAdminBundle\Service\Exception;

use Cwd\GenericBundle\Exception\ServiceException;

/**
 * TranslationNotFoundException
 *
 * @author Ludwig Ruderstaller <lr@cwd.at>
 */
class TranslationNotFoundException extends ServiceException
{

}