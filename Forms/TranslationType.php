<?php
/*
 * This file is part of CwdTranslationAdminBundle.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Cwd\TranslationAdminBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class Translation Form
 *
 * @package Cwd\TranslationAdminBundle\Forms
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @DI\Service("form_translation")
 * @DI\Tag("form.type")
 */
class TranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('transKey', 'text')
                ->add('transLocale', 'text', array('data' => 'de'))
                ->add('messageDomain', 'text', array('data' => 'messages'))
                ->add('translation', 'textarea', array('attr' => array('class' => 'markdown')))

                ->add('save', 'submit', ['label' => 'Save']);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('default'),
            'data_class' => 'Asm\TranslationLoaderBundle\Entity\Translation',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_translation';
    }
}