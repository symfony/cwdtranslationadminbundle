<?php
/*
 * This file is part of the WienHolding IT Assets Tool.
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\TranslationAdminBundle\Grids;

use Ali\DatatableBundle\Util\Datatable;
use Cwd\GenericBundle\Grid\Grid;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class Assetgroup
 *
 * @package ITAsset\Bundle\AdminBundle\Grids
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @DI\Service("grid_translationadminbundle_translation")
 */
class Translation extends Grid
{
    /**
     * @param Datatable $datatable
     *
     * @DI\InjectParams({
     *  "datatable" = @DI\Inject("datatable", strict = false)
     * })
     */
    public function __construct(Datatable $datatable)
    {
        $this->setDatatable($datatable);

        return $this->get();
    }

    /**
     * @return Datatable
     */
    public function get()
    {
        $instance = $this;
        $markdown = new \Parsedown();

        $grid = $this->getDatatable()
            ->setEntity('Asm\TranslationLoaderBundle\Entity\Translation', 'x')
            ->setFields(
                [
                    'ID' => 'x.transKey as xid',
                    'Locale' => 'x.transLocale',
                    'Translation' => 'x.translation',
                    '_identifier_'  => 'x.transKey',
                ]
            )
            ->setOrder('x.transKey', 'asc')
            ->setOrder('x.transKey', 'asc')
            ->setRenderers(
                [
                    3 => [
                        'view' => 'CwdAdminAvantBundle:Grid:_actions.html.twig',
                        'params' => [
                            'edit_route'    => 'cwd_translationadmin_default_edit',
                            'delete_route'  => 'cwd_translationadmin_default_delete',
                        ],
                    ],
                ]
            )
            ->setRenderer(
                function (&$data) use ($instance, $markdown)
                {

                }
            )
            ->setHasAction(true);

        $qb = $grid->getQueryBuilder();

        $qb = $grid->getQueryBuilder()->getDoctrineQueryBuilder();
        $qb->andWhere('x.transLocale=?1')
           ->setParameter('1', 'de');
        $grid->getQueryBuilder()->setDoctrineQueryBuilder($qb);

        return $grid;
    }
} 