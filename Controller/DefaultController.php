<?php
/*
 * This file is part of the TranslationAdminBundle for Symfony2
 *
 * (c)2014 Ludwig Ruderstaller <lr@cwd.at>
 *
 * THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED WARRANTY.
 *
 * The MIT License (MIT) - see LICENSE FILE
 */
namespace Cwd\TranslationAdminBundle\Controller;

use Asm\TranslationLoaderBundle\Model\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Cwd\TranslationAdminBundle\Service\Translation as TranslationService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AssetController
 *
 * @package Cwd\TranslationAdminBundle\Controller
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 * @Route("/language")
 */
class DefaultController extends \ITAsset\Bundle\AdminBundle\Controller\Controller
{
    /**
     * @Route("/")
     * @Template()
     * @return array
     */
    public function indexAction()
    {
        return $this->redirect('/language/list');
    }
    /**
     * @param Request $request
     *
     * @Route("/create")
     * @Secure(roles="ROLE_ADMIN")
     * @return array
     */
    public function createAction(Request $request)
    {
        $translation = $this->getService()->getNew();

        return $this->_formHandler($translation, $request);
    }

    /**
     * @param Request $request
     *
     * @Route("/edit/{id}")
     * @Secure(roles="ROLE_ADMIN")
     * @return array
     */
    public function editAction(Request $request)
    {
        try {

            $translation = $this->getTranslationManager()->findTranslationBy(array(
                'transKey' => $request->get('id'),
                'transLocale' => 'de',
                'messageDomain' => 'messages'
            ));

            // Reset Translation Cache:
            #$dir = $this->container->getParameter("kernel.cache_dir");
            #echo $dir;

            #return new Response();

            return $this->_formHandler($translation, $request);
        } catch (AssetNotFoundException $e) {
            $this->flashError(sprintf('Row with ID %s not found', $request->get('id')));

            //return $this->redirect('/translation/list');
        }
    }

    /**
     * @param Request $request
     *
     * @Route("/delete/{id}", name="cwd_translationadmin_default_delete")
     * @Secure(roles="ROLE_ADMIN")
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        try {
            $this->getService()->remove($request->get('id'));
            $this->flashSuccess('Data successfully removed');
        } catch (AssetNotFoundException $e) {
            $this->flashError('Object with this ID not found ('.$request->get('id').')');
        } catch (\Exception $e) {
            $this->flashError('Unexpected Error: '.$e->getMessage());
        }

        return $this->redirect('/assetgroup/list');
    }

    /**
     * @param Translation $object
     * @param Request     $request
     * @param bool        $persist
     *
     * @return array
     */
    protected function _formHandler(Translation $object, Request $request)
    {

        $form = $this->createForm('form_translation', $object);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $object->setDateCreated(new \DateTime());
                $object->setDateUpdated(new \DateTime());
                $this->getService()->persist($object);
                $this->getService()->getEm()->flush();
                $this->clearLanguageCache();
                $this->flashSuccess('Data successfully saved');

                return $this->redirect('/language/list');
            } catch (\Exception $e) {
                $this->flashError('Error while saving Data: '.$e->getMessage());
            }

        }

        return $this->render('CwdTranslationAdminBundle:Default:form.html.twig', array(
            'form'  => $form->createView(),
            'title' => 'Translation',
            'icon'  => 'fa fa-flag'
        ));
    }

    /**
     * Remove language in current cache directories
     */
    private function clearLanguageCache()
    {
        $cacheDir =  $this->container->getParameter("kernel.cache_dir");
        $finder = new \Symfony\Component\Finder\Finder();
        $finder->in(array($cacheDir . "/translations"))->files();

        foreach ($finder as $file) {
            unlink($file->getRealpath());
        }
    }

    /**
     * @Route("/list")
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        $this->get('grid_translationadminbundle_translation');

        return array();
    }

    /**
     * Grid action
     *
     * @Route("/grid")
     * @return Response
     */
    public function gridAction()
    {
        return $this->get('grid_translationadminbundle_translation')->execute();
    }

    /**
     * @return Translation
     */
    protected function getService()
    {
        return $this->get('service_translation');
    }

    /**
     * @return TranslationManager
     */
    protected function getTranslationManager()
    {
        return $this->get('asm_translation_loader.translation_manager');
    }
}
